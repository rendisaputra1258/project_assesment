<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>
    <h1 class="text-center">AROMA KARSA</h1>
    <div class="container">
        <div class="col">
            <div class="row">
       
           <b> Dari sebuah lontar kuno, Raras Prayagung mengetahui <br> bahwa Puspa Karsa yang dikenalnya sebagai dongeng, ternyata tanaman sungguhan yang tersembunyi di tempat rahasia.

Obsesi Raras memburu Puspa Karsa, <br> bunga sakti yang konon mampu mengendalikan kehendak dan cuma bisa <br> diidentifikasi melalui aroma, mempertemukannya dengan Jati Wesi.

Jati memiliki penciuman luar biasa. <br> Di TPA Bantar Gebang, tempatnya tumbuh besar, <br> ia dijuluki si Hidung Tikus. <br> Dari berbagai pekerjaan yang dilakoninya untuk bertahan hidup, satu yang paling Jati banggakan, yakni meracik parfum.

Kemampuan Jati memikat Raras. <br> Bukan hanya mempekerjakan Jati di perusahaannya, Raras ikut mengundang <br> Jati masuk ke dalam kehidupan pribadinya. Bertemulah Jati dengan Tanaya Suma, anak tunggal Raras, yang memiliki kemampuan serupa dengannya.

Semakin jauh Jati terlibat <br> dengan keluarga Prayagung dan Puspa Karsa, semakin banyak <br> misteri yang ia temukan, tentang dirinya dan masa lalu yang tak pernah ia tahu.
</b>   
<br>
          
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col">
            <a href="tentang.php" class="btn btn-primary">kembali</a>
        </div>
    </div>
</div>
   
    

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>