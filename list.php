<?php

include 'connection.php';
include 'cari.php';


foreach ($data_buku as $key) {
    
}

//search pencarian//
$containt=[];
if(isset($_POST['cari']))
{
  $containt=searching($_POST['search']);
}
if (!empty($containt))
{
  $data_buku=$containt;
}


if(isset($_POST['search']))
{
    
    $name=$_POST['search'];
    
    $search=$db->prepare("select * from buku where judul=? or penulis=? or genre=? or jurusan=?");
    
    $search->bindValue(1, $name,PDO::PARAM_STR);
    $search->bindValue(2, $name,pdo::PARAM_STR);
    $search->bindValue(3, $name,PDO::PARAM_STR);
    $search->bindValue(4, $name,pdo::PARAM_STR);
    
    $search->execute();
    
    $tampil_data=$search->fetchAll();
    
    $row=$search->rowCount();
}

// var_dump($data_buku);


$fiks_data=[];
if(isset($_POST['filter']))
{
    $filter=$_POST['filter'];
    if($filter == "")
    {
        $fiks_data=$data_buku;
    }else{
        foreach($data_buku as $key)
        {
            if($key[0] == $filter){
                $fiks_data[]=[$key[0],$key[1],$key[2]];
            }
        }
    }
}else{
    $fiks_data=$data_buku;
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="fontawesome-free/css/all.min.css">
        <title>DAFTAR PESERTA</title>
      </head>
    <body class=" bg-info mt-5">
    <nav class="navbar fixed-top navbar-expand navbar-dark bg-dark">
    <div class="container">
     <a class="navbar-brand"><img src="image/buku2.png" width="65" alt=""></a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
     <span class="navbar-toggler-icon"></span>
      </button>
     <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
     <div class="navbar-nav">
      <a class="nav-link active" href="daftar.php">Daftar <span class="sr-only">(current)</span></a>
      <a class="nav-link active" href="list.php">Data peserta <span class="sr-only">(current)</span></a>
      <a class="nav-link active" href="tentang.php">Example<span class="sr-only">(current)</span></a>
     </div>
     <form class="form-inline my-2 my-lg-0 ml-auto" action="list.php" method="POST">
        <input class="form-control mr-sm-2" type="search" name="search" placeholder="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" name="cari" type="submit">Search</button>
     </form>
    </div>
  </div>
    <a class="nav-link active text-light" href="index.php">Keluar<i class="fas fa-sign-out-alt"></i></a>
       </nav>            
        <br>
        <br>
     <div class="col">              
       <div class="container">  
         <?php if(isset($row)):?>
              <div class="alert alert-primary alert-dismissible fade show mt-3" role="alert">
                <p class="lead"><?php echo $row; ?>Data Ditemukan</p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <?php endif;?>
       <div class="text-center">
         <div class="bg-secondary">
            <h1 class="text-light" style="font-family: calibry;">Data peserta</h1>                
                <table class="table table-striped btn-light">
                   <thead class="bg bg-light">
                <tr >
             <th scope="col">ID BUKU</th>
            <th scope="col">JUDUL</th>
            <th scope="col">PENULIS</th>
            <th scope="col">GENRE</th>
            <th scope="col">JURUSAN</th>
            <th scope="col">AKSI</th>
         </tr>
       </thead>
     </div>
    </div>
  </div>
    <tbody>
        <?php foreach ($data_buku as $key):?>
            <tr>
                <td><?php echo $key['id_buku'];?></td>
                <td><?php echo $key['judul'];?></td>
                <td><?php echo $key['penulis'];?></td>
                <td><?php echo $key['genre'];?></td>
                <td><?php echo $key['jurusan'];?></td>   
             <td><a onclick="return confirm ('apakah anda ingin mengapus data ini?')" href="delete.php?id_buku=<?php echo $key['id_buku'];  ?>"  class="btn btn-danger">Hapus</a> 
                 <a  href="edit.php?id_buku=<?php echo $key['id_buku']; ?>"  class="btn btn-primary">Edit</a></td>
            </tr>   
            <?php endforeach; ?>
        </tbody> 
    </table>
    <footer>
    <!-- end from input buku -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    
</body>
</html>